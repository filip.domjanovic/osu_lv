import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler


data=pd.read_csv('data_C02_emission.csv')

data=data.drop(["Make","Model"], axis=1)

input_variables = ["Fuel Consumption City (L/100km)",
                   "Fuel Consumption Hwy (L/100km)",
                   "Fuel Consumption Comb (L/100km)",
                   "Fuel Consumption Comb (mpg)",
                   "Engine Size (L)",
                   "Cylinders"]

output_variable = ["C02 Emissions (g/km)"]
x = data[input_variables].to_numpy()
y = data[input_variables].to_numpy()

X_train , X_test , y_train , y_test = train_test_split (x , y , test_size = 0.2 , random_state =1 )

plt.scatter(X_train[:,0],y_train,c="blue")
plt.scatter(X_test[:,0],y_test,c="red")
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("C02 Emissions (g/km)")
plt.show()

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

plt.subplot(2, 1, 1)
plt.hist(X_train[:, 0])
plt.title('Before Scaling')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('Frequency')

plt.subplot(2, 1, 2)
plt.hist(X_train_scaled[:, 0])
plt.title('After Scaling')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('Frequency')
plt.show()