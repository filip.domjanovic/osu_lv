import pandas as pd
import matplotlib.pyplot as plt
data = pd.read_csv('data_C02_emission.csv')

print(len(data))
print(data.dtypes)
print(data.isnull().sum())
data.dropna(axis=0)
data.drop_duplicates()
data = data.reset_index(drop=True)
for col in ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']:
    data[col]=data[col].astype('category')
print(data.dtypes)


emission = data.sort_values(
    by="Fuel Consumption City (L/100km)", ascending=False
)
print(emission.head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print(emission.tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

sizeofmotor = data[(data['Engine Size (L)']>2.4) & (data['Engine Size (L)']<3.6)]
print(len(sizeofmotor))
print(sizeofmotor["CO2 Emissions (g/km)"].mean())

audis = data[(data['Make']=='Audi')]
print(len(audis))

audisavg = data[(data['Make']=='Audi') & (data['Cylinders']==4)]
print(audisavg["CO2 Emissions (g/km)"].mean())


print(data['Cylinders'].unique())
vozs4 = data[(data['Cylinders']==4)]
print(len(vozs4))
vozs6 = data[(data['Cylinders']==6)]
print(len(vozs6))
vozs8 = data[(data['Cylinders']==8)]
print(len(vozs8))
vozs12 = data[(data['Cylinders']==12)]
print(len(vozs12))
vozs10 = data[(data['Cylinders']==10)]
print(len(vozs10))
vozs5 = data[(data['Cylinders']==5)]
print(len(vozs5))
vozs16 = data[(data['Cylinders']==16)]
print(len(vozs16))
vozs3 = data[(data['Cylinders']==3)]
print(len(vozs3))

print(vozs4["CO2 Emissions (g/km)"].mean())
print(vozs6["CO2 Emissions (g/km)"].mean())
print(vozs8["CO2 Emissions (g/km)"].mean())
print(vozs12["CO2 Emissions (g/km)"].mean())
print(vozs10["CO2 Emissions (g/km)"].mean())
print(vozs5["CO2 Emissions (g/km)"].mean())
print(vozs16["CO2 Emissions (g/km)"].mean())
print(vozs3["CO2 Emissions (g/km)"].mean())


dizel = data[(data["Fuel Type"]=='D')]
regbenz = data[(data["Fuel Type"]=='X')]

print(dizel["Fuel Consumption City (L/100km)"].mean())
print(regbenz["Fuel Consumption City (L/100km)"].mean())
print(dizel["Fuel Consumption City (L/100km)"].median())
print(regbenz["Fuel Consumption City (L/100km)"].median())

najv= data[(data["Fuel Type"]=='D') & (data["Cylinders"]==4).max()]
vozilo=najv.sort_values(by=["Fuel Consumption City (L/100km)"]).tail(1)

print(vozilo[["Make", "Model"]])

rucnimj = data[(data["Transmission"].str.startswith("M"))]
print(len(rucnimj))

print ( data.corr(numeric_only=True) )