import numpy as np
import matplotlib . pyplot as plt
from sklearn . metrics import accuracy_score
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay, classification_report

class sklearn.linear_model . LogisticRegression ( penalty =’ l2 ’, *,
dual = False , tol =0 . 0001 , C=1 .0 , fit_intercept = True ,
intercept_scaling =1 , class_weight = None , random_state = None ,
solver =’ lbfgs ’, max_iter =100 , multi_class =’ auto ’, verbose =0 ,
warm_start = False , n_jobs = None , l1_ratio = None )

# stvarna vrijednost izlazne velicine i predikcija
y_true = np . array ([1 , 1 , 1 , 0 , 1 , 0 , 1 , 0 , 1])
y_pred = np . array ([0 , 1 , 1 , 1 , 1 , 0 , 1 , 0 , 0])

# tocnost
print (" Tocnost : " , accuracy_score ( y_true , y_pred ) )

# matrica zabune
cm = confusion_matrix ( y_true , y_pred )
print (" Matrica zabune : " , cm )
disp = ConfusionMatrixDisplay ( confusion_matrix ( y_true , y_pred ) )
disp . plot ()
plt . show ()

# report
print ( classification_report ( y_true , y_pred ) )