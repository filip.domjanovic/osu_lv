import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay, classification_report



X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

plt.figure()
plt.scatter(X_train[:,0], X_train[:,1], cmap='copper', c=y_train)
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker='x', cmap='winter')

logreg_model=LogisticRegression()
logreg_model.fit(X_train,y_train)

y_test_p = logreg_model.predict_proba(X_test)
print(y_test_p)

b=logreg_model.intercept_[0]
w1,w2 =logreg_model.coef_.T

c=-b/w2
m=-w1/w2

xmin, xmax = -4, 4
ymin, ymax = -4, 4

xd=np.array([xmin,xmax])
yd=m+xd+c
