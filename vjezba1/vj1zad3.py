list = []
while(1):
    inp=input()
    if inp=="Done":
        break
    try:
        i=int(inp)
        list.append(int(i))
    except ValueError:
        print("Unesi cijeli broj za pohranu u listu ili Done za zavrsetak")
mean=sum(list)/len(list)
list.sort()
print(f"Broj brojeva: {len(list)}\nSrednja vrijednost: {mean}\nMinimalna vrijednost: {min(list)}\nMaksimalna vrijednost: {max(list)}\nSortirana lista: {list}")
