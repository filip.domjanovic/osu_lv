fhand = open ("SMSSpamCollection.txt")

hams = []
spams = []
br = 0

for line in fhand :
    line = line.rstrip()
    words = line.split()
    if(words[0] == "ham"):
        hams.append(len(words) - 1)
    elif(words[0] == "spam"):
        spams.append(len(words) - 1)
        if(line[len(line) - 1] == "!"):
            br += 1
fhand.close()

avgHam = sum(hams)/len(hams)
avgSpam = sum(spams)/len(spams)

print(f"Average ham: {avgHam}\nAverage spam: {avgSpam}\nUskličnici spam: {br}")