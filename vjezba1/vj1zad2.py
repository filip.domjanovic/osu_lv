try:
    print("unesi broj u intervalu od 0.0 do 1.0")
    broj = float(input())
    if broj>1 or broj<0:
        print("Broj nije u zadanom intervalu")
    elif broj>=0.9:
        print("A")
    elif broj>=0.8:
        print("B")
    elif broj>=0.7:
        print("C")
    elif broj>=0.6:
        print("D")
    else:
        print("F")
except ValueError:
    print("Unesena vrijednost mora biti broj")