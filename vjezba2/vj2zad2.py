import numpy as np
import matplotlib . pyplot as plt

data=np.genfromtxt("data.csv", delimiter=',', skip_header=1)

rows,cols=np.shape(data)
print(f"Na {rows} ljudi")
height=data[:,1]
weight=data[:,2]

plt.scatter(weight,height)
plt.xlabel ('Masa')
plt . ylabel ('Visina')
plt . title ('Odnos visine i mase')
plt.show()

height50=height[::50]
weight50=weight[::50]
plt.scatter(weight50,height50)
plt.xlabel ('Masa')
plt . ylabel ('Visina')
plt . title ('Odnos visine i mase')
plt.show()

print(f"Minimalna tezina {np.min(weight)}\nMaksimalna tezina {np.max(weight)}\nMinimalna visina {np.min(height)}\nMaksimalna visina{np.max(height)}\nSrednja tezina {np.mean(weight)}\nSrednja visina {np.mean(height)}")

m=data[np.where(data[:,0]==1)]
w=data[np.where(data[:,0]==0)]




print(f"Minimalna visina m {np.min(m[:,1])}\nMinimalna visina w {np.min(w[:,1])}\nMaksimalna visina m {np.max(m[:,1])}\nMaksimalna visina z {np.max(w[:,1])}\nProsjecna visina m {np.mean(m[:,1])}\nProsjecna visina w {np.mean(w[:,1])}")
